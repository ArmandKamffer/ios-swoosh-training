//
//  SkillViewController.swift
//  Swoosh
//
//  Created by Armand Kamffer on 2018/10/23.
//  Copyright © 2018 Armand Kamffer. All rights reserved.
//

import UIKit

class SkillViewController: UIViewController {
    //*******************************
    //MARK:- Class Variables
    var player: Player!
    
    //*******************************
    //MARK:- Lifecycle Hooks
    override func viewDidLoad() {
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
