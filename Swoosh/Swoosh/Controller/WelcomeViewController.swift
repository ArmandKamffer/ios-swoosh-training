//
//  ViewController.swift
//  Swoosh
//
//  Created by Armand Kamffer on 2018/09/20.
//  Copyright © 2018 Armand Kamffer. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    //*******************************
    //MARK:- IBOutlets
    @IBOutlet weak var swooshImage: UIImageView!
    @IBOutlet weak var bgImage: UIImageView!
    
    //*******************************
    //MARK:- Lifecycle Hooks
    override func viewDidLoad() {
        super.viewDidLoad()

        swooshImage.frame = CGRect(x: view.frame.size.width / 2 - swooshImage.frame.size.width / 2, y: 50, width: swooshImage.frame.size.width, height: swooshImage.frame.size.height)
        
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //*******************************
    //MARK:- Navigation
    @IBAction func unwindFromSkillVC(unwindSegue: UIStoryboardSegue) {}
}

