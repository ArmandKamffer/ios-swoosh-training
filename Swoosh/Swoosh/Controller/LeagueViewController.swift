//
//  LeagueViewController.swift
//  Swoosh
//
//  Created by Armand Kamffer on 2018/10/23.
//  Copyright © 2018 Armand Kamffer. All rights reserved.
//

import UIKit

class LeagueViewController: UIViewController {
    //*******************************
    //MARK:- IBOutlets
    @IBOutlet weak var nextBtn: BorderButton!
    
    //*******************************
    //MARK:- Class Variables
    var player: Player!
    
    //*******************************
    //MARK:- Lifecycle Hooks
    override func viewDidLoad() {
        super.viewDidLoad()

        player = Player()
        setNeedsStatusBarAppearanceUpdate()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //*******************************
    //MARK:- Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SkillViewController {
            vc.player = player
        }
    }
    
    //*******************************
    //MARK:- IBActions
    @IBAction func nextBtnPressed(_ sender: BorderButton) {
        performSegue(withIdentifier: "SkillVCSegue", sender: self)
    }
    
    @IBAction func mensBtnPressed(_ sender: Any) {
        setDesiredLeague(desiredLeague: "Mens")
    }
    
    @IBAction func womensBtnPressed(_ sender: Any) {
        setDesiredLeague(desiredLeague: "Womens")
    }
    
    @IBAction func coedBtnPressed(_ sender: Any) {
        setDesiredLeague(desiredLeague: "Co-Ed")
    }
    
    func setDesiredLeague(desiredLeague: String) {
        player.desiredLeague = desiredLeague
        nextBtn.isEnabled = true
    }
}
