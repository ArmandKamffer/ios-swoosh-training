//
//  Player.swift
//  Swoosh
//
//  Created by Armand Kamffer on 2018/10/23.
//  Copyright © 2018 Armand Kamffer. All rights reserved.
//

import Foundation

struct Player {
    var desiredLeague: String?
    var skillLevel: String?
}
