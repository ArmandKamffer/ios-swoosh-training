//
//  BorderButton.swift
//  Swoosh
//
//  Created by Armand Kamffer on 2018/09/20.
//  Copyright © 2018 Armand Kamffer. All rights reserved.
//

import UIKit

class BorderButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.borderColor = UIColor.white.cgColor;
        layer.borderWidth = 2.0
    }
    
}
