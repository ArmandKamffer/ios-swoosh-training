# iOS-Swoosh-Training

This project serves as the code base for a training app built in the Devslopes iOS 12 course.

## Description

The app is a simple app built to introduce Auto Layout concepts and to navigate to different ViewControllers using the storyboard and programatically. Built using Swift 4.2.

## Final App

<kbd>
<img src="docs/View1.png" width="400px"/>
</kbd>

<kbd>
<img src="docs/View2.png" width="400px"/>
</kbd>

<kbd>
<img src="docs/View3.png" width="400px"/>
</kbd>